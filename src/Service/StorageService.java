package Service;

import java.util.ArrayList;
import java.util.List;

import com.example.servletjspdemo.domain.Person;

public class StorageService {
	
	private List<Person> db = new ArrayList<Person>();
	
	public void add(Person person){
		Person newPerson = new Person(person.getAddresstype(), person.getProvince(), person.getCity(), person.getNumber(), person.getPostcode(), person.getCity());
		db.add(newPerson);
	}
	
	public List<Person> getAllPersons(){
		return db;
	}

}
