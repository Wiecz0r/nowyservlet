package Servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/form")
public class FormServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		out.println("<html><body><h2>Simple form servlet</h2>" +
				"<form action='data'>" +
				"Address Type:<select name='addresstype'>" +
				  "<option value='registration of residence'>Registration of Residence</option>" + 
				  "<option value='mail drop'>Mail Drop</option>"  +
				  "<option value='job'>Job</option>"  +
				"</select> <br />"+
				"Province:<select name='province'>" +
				  "<option value='pomorski'>Pomorski</option>" + 
				  "<option value='warminsko-mazurskie'>Warmińsko-Mazurskie</option>"  +
				  "<option value='zachodnio-pomorskie'>Zachodnio-Pomorskie</option>"  +
				"</select> <br />"+
				"City: <input type='text' name='city' /> <br />" +
				"Post Code: <input type='text' name='postcode' /> <br />" +
				"Street: <input type='text' name='street' /> <br />" +
				"Home Number/Flat Number: <input type='text' name='number' /> <br />" +
				"<input type='submit' value=' OK ' />" +
				"</form>" +
				"</body></html>");
		out.close();
	}

}
