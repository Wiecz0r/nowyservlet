package Servlet;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/data")
public class DataServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String selectedOccupation = "";
		for (String occupation : request.getParameterValues("occupation")) {
			selectedOccupation += occupation + " ";
		}
		out.println("<html><body><h2>Your data</h2>" +
				"<p>Drop Box Test: " + request.getParameter("addresstype") + "<br />" +
				"<p>Drop Box Test: " + request.getParameter("province") + "<br />" +
				"<p>City: " + request.getParameter("city") + "<br />" +
				"<p>Post Code: " + request.getParameter("postcode") + "<br />" +
				"<p>Street: " + request.getParameter("street") + "<br />" +
				"<p>Number: " + request.getParameter("number") + "<br />" +
				"</body></html>");
		out.close();
	}

}