package Servlet;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/form")
public class FormServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		out.println("<html><body><h2>Simple form servlet</h2>" +
				"<form action='data'>" +
				"First name: <input type='text' name='name' /> <br />" +
				"Surname: <input type='text' surname='surname' /> <br />" +
				"E-mail: <input type='text' email='email' /> <br />" +
				"Validate E-mail: <input type='text' emailValidate='emailValidate' /> <br />" +
				"<input type='checkbox' name='occupation' value='nothing'>Jobless<br />" +
				"<input type='checkbox' name='occupation' value='studying'>Student<br />" +
				"<input type='checkbox' name='occupation' value='this'>I dont know<br />" +
				"<input type='checkbox' name='occupation' value='that'>Not today<br />" +
"<select name='operator'>" +
  "<option value='volvo'>Volvo</option>" + 
  "<option value='saab'>Saab</option>"  +
  "<option value='opel'>Opel</option>"  +
  "<option value='audi'>Audi</option>"  +
"</select>" +
				"<input type='submit' value=' OK ' />" +
				"</form>" +
				"</body></html>");
		out.close();
	}

}