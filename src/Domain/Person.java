package Domain;

public class Person {
	private String addresstype;
	private String province;
	private String city;
	private String postcode;
	private String street;
	private String number;
	
	public Person(java.lang.String addresstype, java.lang.String province, java.lang.String city, java.lang.String postcode, java.lang.String street, java.lang.String number){
		this.addresstype = addresstype;
		this.province = province;
		this.city = city;
		this.postcode = postcode;
		this.street = street;
		this.number = number;
	}

	public String getAddresstype() {
		return addresstype;
	}

	public void setAddresstype(String addresstype) {
		this.addresstype = addresstype;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	
}